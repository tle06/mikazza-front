locals {
  environment = lookup(var.workspace_to_environment_map, var.ENV, "stage")
}

/* -------------------------------------------------------------------------- */
/*                             environment mapping                            */
/* -------------------------------------------------------------------------- */

variable "workspace_to_environment_map" {
  description = "The map between workspace and environment"
  type        = map(any)
  default = {
    stage = "stage"
    prod  = "prod"
  }
}

variable "terraform-name-prefix" {
  type        = string
  description = "The prefix that we will be added to the ressource name created by terraform"
  default     = "tf"
}

variable "azure-rg-name-prefix" {
  type        = string
  description = "The prefix that will be added to Azure ressource"
  default     = "az"
}

variable "azure-default-location" {
  type        = string
  description = "The Azure default location"
  default     = "France Central"
}

variable "ENV" {
  description = "The ENV value"
  type        = string
}