import React, { useEffect }  from 'react';
import { useDispatch } from 'react-redux';
import { logout } from '../store/userSlice'
import { useHistory, useParams, useLocation } from "react-router-dom";
import { Toolbar, Divider, makeStyles, Drawer, List, ListItem, ListItemText, ListItemIcon, Typography, Link } from '@material-ui/core'
import logo from '../assets/logo/building-white-on-green.svg';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    minHeight: '50px'
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: 'auto',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  listItem: {
    minHeight: '50px', // green if tab active
  }
}));

export function ClippedDrawer(props) {
  const classes = useStyles();
  const history = useHistory();
  const location = useLocation();
  const dispatch = useDispatch();

  useEffect(() => {
    console.log('Location changed', location.pathname);
  }, [location]);

  const pathname = location.pathname
  const id = pathname.replace('/property/', '').replace(/\/.*$/g, '') //.replace(/\D+/g, '')
  console.log('id',id) 
  let isProperty = false

  if (pathname.indexOf('create') < 0 && pathname.indexOf('edit') < 0) {
    isProperty = pathname.includes('/property/')
  }




  const createProperty = () => {
    history.push(`/property/create`)
  }

  const showTab = (tab) => {
    history.push(`/property/${id}${ tab ? '/' : ''}${tab}`)
  }

  const handleClick = () => {
    try {
     history.push(`/home`)
    } catch(error) {
      console.log(error)
    }
  }

  const logoutUser = () => {
    dispatch(logout())
  }

  const renderPropertyTabs = (
    <List>
      <ListItem button key={'Details'} onClick={e => showTab('details', e)} classes={{root: classes.listItem}} style={{ color: pathname.includes('details') ? '#10d48e' : ''  }}>
        PROPERTY DETAILS
      </ListItem>
      <ListItem button key={'Documents'} onClick={e => showTab('documents', e)} classes={{root: classes.listItem}} style={{ color: pathname.includes('documents') ? '#10d48e' : ''  }}>
        DOCUMENTS            
      </ListItem>
      <ListItem button key={'Contacts'} onClick={e => showTab('contacts', e)} classes={{root: classes.listItem}} style={{ color: pathname.includes('contacts') ? '#10d48e' : ''  }}>
        CONTACTS
      </ListItem>
    </List>
  )

  return (
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Toolbar />
        <div className={classes.drawerContainer}>
          <Typography variant="h6" className={classes.title}>
            <Link href="/" onClick={handleClick} style={{ textDecoration: 'none' }}>
                <img src={logo} alt={"logo"} height= "40px" /> MIKAZZA
            </Link>
          </Typography>
          <List>
            <ListItem button key={'All properties'} onClick={handleClick} classes={{root: classes.listItem}} style={{ color: pathname.includes('home') ? '#10d48e' : ''  }}>
              ALL PROPERTIES
            </ListItem>
            <ListItem button key={'Create property'} onClick={createProperty} classes={{root: classes.listItem}} style={{ color: pathname.includes('create') ? '#10d48e' : ''  }}>
              CREATE A NEW PROPERTY
            </ListItem>
          </List>
          <Divider />
          { isProperty ? renderPropertyTabs : null}
        </div>
      </Drawer>
  );
}
