import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
//import { forceReducerReload } from 'redux-injectors';
import reducer from './reducers'
// import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
// import propertyReducer from './propertySlice'
// import apiMiddleware from './middlewares/api';
// // import { fetchState } from './persistState';
// // const savedState = fetchState();

// const store = configureStore({
//     reducer: {
//       property: propertyReducer
//     },
//     middleware: [apiMiddleware, ...getDefaultMiddleware()]
//     //     savedState
// })

// export default store

// // const createReduxStore = () => {
// //   const store = configureStore({
// //     reducer: rootReducer, 
// //     middleware: [apiMiddleware, ...getDefaultMiddleware()], 
// //     savedState
// //   })
// //   return store;
// // };

// // export default createReduxStore;

const store = configureStore({
  reducer,
  middleware: [...getDefaultMiddleware()]
})

export default store
