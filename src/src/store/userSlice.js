import { createSlice } from '@reduxjs/toolkit'
import api from './middlewares/api'

// Local storage
const initialUser = localStorage.getItem('user')
// const initialUser = useSelector(state => state.user.user)

// Slice
const userSlice = createSlice({
  name: 'user',
  initialState: {
    user: initialUser,
    token: null
  },
  reducers: {
    authSuccess: (state,  { payload }) => {
      let {email, token} = payload;
      state.user = email;
      state.token = token;
      localStorage.setItem('user', email)
      localStorage.setItem('token', token)
    },
    logoutSuccess: (state, action) =>  {
      state.user = null;
      state.token = null
      localStorage.removeItem('user')
      localStorage.removeItem('token')
    },
  },
});

export default userSlice.reducer

// Actions
const { authSuccess, logoutSuccess } = userSlice.actions

export const register = ({ email, password }) => async dispatch => {
  try {
    const { data } = await api.post('/auth/register', { email, password })
    const token = data.Authorization
    dispatch(authSuccess({email, token}))
  } catch (e) {
    return console.error(e.message)
  }
}

export const login = ({ email, password }) => async dispatch => {
  try {
    const res = await api.post('/auth/login', { email, password })
    const token = res.data.Authorization
    dispatch(authSuccess({email, token}))
  } catch (e) {
    return console.error(e.message)
  }
}

export const logout = () => async dispatch => {
  try {
    await api.post('/auth/logout')
    return dispatch(logoutSuccess())
  } catch (e) {
    return console.error(e.message);
  }
}