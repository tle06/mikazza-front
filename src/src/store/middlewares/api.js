import axios from "axios"
import jwtDecode from 'jwt-decode'

const baseURL = process.env.REACT_APP_BASE_URL || "/api/v1/"

const api = axios.create({
    baseURL,
    headers: {
        'Content-Type': 'application/json'
    }
})
api.interceptors.request.use(
    config => {
        let token = localStorage.getItem('token')
        if (token) {
            const { exp } = jwtDecode(token)
                // Refresh the token a minute early to avoid latency issues
            const expirationTime = (exp * 1000) - 60000
            if (Date.now() >= expirationTime) {
                console.log('token has expired', Date.now() >= expirationTime)
                token = null
                    // localStorage.setItem('token', token)
                localStorage.clear()
            }
            config.headers['Authorization'] = token
        }
        return config
    },
    error => {
        Promise.reject(error)
    }
)

export default api